# Email API
Simple API for SMTP accounts administrations and Email sending 

### List configured accounts
    GET http://{{base_path}}/accounts`

Outputs a JSON array. Eg:

    [
         "test5",
         "test4",
         "test"
    ]


### Add new SMTP accounts
    POST http://{{base_path}}/accounts`

Account details are send as a JSON string in BODY. Eg.

    {
        "Host":"smtp.domain.com",
        "SMTPAuth":1,
        "Username":"sendmail@domain.com",
        "Password":"12345",
        "Security":"tls",
        "Port":587,
        "id": "test"
    }

Responds with
- `400` and an error message if the input data is faulty
- `201 CREATED` if the account has been created
- `409 CONFLICT` if is there is already an account defined with the same `id` 
- `500` if there are any server side errors

### Get SMTP account details
    POST http://{{base_path}}/accounts/{{id}}`

Retrieves the account details as a JSON String. Eg.
    {
        "Host":"smtp.domain.com",
        "SMTPAuth":1,
        "Username":"sendmail@domain.com",
        "Password":"*****",
        "Security":"tls",
        "Port":587
    }

### Update SMTP account details
    PUT http://{{base_path}}/accounts/{{id}}`
Account details are send as a JSON string in BODY. See Account creation .

Responds with
- `400` and an error message if the input data is faulty
- `204 NO CONTENT` if the account has been updated
- `409 CONFLICT` if is there is already an account defined with the same `id` 
- `500` if there are any server side errors

### Delete SMTP account details
    DELETE http://{{base_path}}/accounts/{{id}}`
Removes an SMTP account.

Responds with
- `204 NO CONTENT` if the account has been delete
- `404 CONFLICT` if account `id` does not exist 
- `500` if there are any server side errors

### Send Email
    POST http://{{base_path}}/accounts/{{id}}/send`
Email details are send as a JSON string in BODY. Eg.

    {
        "to":"receiver1@domain.com",
        "from":"sender@mydomain.net",
        "subject":"Some subject",
        "body":"<p>HTML Body</p>",
        "body_alt":"Text body",
    }

Different emails can be sent with a single call, by grouping the data in an array. Eg

    [
        {
            "to":"receiver1@domain.com",
            "from":"sender@mydomain.net",
            "subject":"Some subject",
            "body":"<p>HTML Body</p>",
            "body_alt":"<p>HTML Body</p>",
        },
        {
            "to":"receiver2@domain.com",
            "from":"sender@mydomain.net",
            "subject":"Other subject",
            "body":"Sendmail 1"
        }
    ]


 