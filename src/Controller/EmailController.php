<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
class EmailController
{
    private $accountsPath = "../var/data/mailer";

    /**
     * @Route("/accounts/{id}/send", methods={"POST"})
     * @param $id
     * @param null $input
     * @return Response
     */
    public function send($id,$input=null)
    {
        if(!file_exists($this->accountsPath."/$id.php"))
            return new Response("Account not found",404);

        // load account config
        $cfg = require $this->accountsPath."/$id.php";

        // read input
        $request = Request::createFromGlobals();
        if(!$input)
            $input = json_decode($request->getContent());

        if(!is_array($input))
            $input = [$input];

        $mail = new PHPMailer(true);

        try {
            //            $mail->SMTPDebug = SMTP::DEBUG_CONNECTION;
            //Server settings
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = $cfg["Host"];                    // Set the SMTP server to send through
            $mail->SMTPAuth = $cfg["SMTPAuth"];                                   // Enable SMTP authentication
            $mail->Username = $cfg["Username"];                     // SMTP username
            $mail->Password = $cfg["Password"];                               // SMTP password
            $mail->SMTPSecure = $cfg["SMTPSecure"];         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = $cfg["Port"];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            foreach ($input as $msg) {
                //Recipients
                if (property_exists($msg,"from"))
                    $mail->setFrom($msg->from);
                else
                    $mail->setFrom($cfg["Username"], 'Mailer');

                $mail->clearAddresses();
                $mail->addAddress($msg->to);     // Add a recipient

                // Content
                // Set email format to HTML
                if(isset($msg->isHTML) && $msg->isHTML)
                    $mail->isHTML(true);
                $mail->Subject = $msg->subject;
                if (property_exists($msg,"body"))
                    $mail->Body = $msg->body;
                if (property_exists($msg,"body_alt"))
                    $mail->AltBody = $msg->body_alt;

                $mail->send();
            }

            return new Response(
                json_encode(["message"=>"Email has been sent"]),
                200,
                ["Content-Type"=> "application/json"]
            );
        } catch (Exception $e) {
            return new Response(json_encode(["error"=>$mail->ErrorInfo]),500);
        }
    }

    /**
     * @Route("/accounts/", methods={"GET"})
     */
    public function list_accounts()
    {
        $accounts = [];
        $d = opendir($this->accountsPath);
        while ($read=readdir($d)) {
            if(in_array($read,[".",".."]))
                continue;
            $accounts[] = substr($read,0,strlen($read)-4);
        };
        closedir($d);
        return new Response(json_encode($accounts),200,["Content-Type"=>"application/json"]);
    }

    /**
     * @Route("/accounts/{id}", methods={"GET"})
     */
    public function get_account($id)
    {
//        echo $this->accountsPath."/$id.php";
        if(!file_exists($this->accountsPath."/$id.php"))
            return new Response("Account not found",404);

        $cfg = require $this->accountsPath."/$id.php";
        if(isset($cfg["Password"]))
            $cfg["Password"] = "******";
        return new Response(json_encode($cfg),200,["Content-Type"=>"application/json"]);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/accounts/{id}", methods={"DELETE"})
     */
    public function delete_account($id)
    {
        // echo $this->accountsPath."/$id.php";
        if(!file_exists($this->accountsPath."/$id.php"))
            return new Response("Account not found",404);
        if(@unlink($this->accountsPath."/$id.php"))
            return new Response(null,204);
        else
            return new Response(json_encode(["error"=>"could not delete account"]),500,["Content-Type"=>"application/json"]);
    }

    /**
     * @param $cfg
     * @throws Exception
     */
    public function smtp_check($cfg)
    {

        date_default_timezone_set('Etc/UTC');

        $smtp = new SMTP;

//        $smtp->do_debug = SMTP::DEBUG_CONNECTION;

        try {
            //Connect to an SMTP server
            if (!$smtp->connect($cfg["Host"], $cfg["Port"])) {
                throw new Exception('Connect failed',500);
            }

            //Say hello
            if (!$smtp->hello(gethostname())) {
                throw new Exception('EHLO failed: ' . $smtp->getError()['error'],500);
            }
            $e = $smtp->getServerExtList();
            if(isset($cfg["SMTPSecure"]) && $cfg["SMTPSecure"]=="tls") {
                $tlsok = $smtp->startTLS();
                if (!$tlsok) {
                    throw new Exception('Failed to start encryption: ' . $smtp->getError()['error'],500);
                }
                //Repeat EHLO after STARTTLS
                if (!$smtp->hello(gethostname())) {
                    throw new Exception('EHLO (2) failed: ' . $smtp->getError()['error'],500);
                }
                //Get new capabilities list, which will usually now include AUTH if it didn't before
                $e = $smtp->getServerExtList();
            }

            if(isset($cfg["Username"])) {
                if ($smtp->authenticate($cfg["Username"], $cfg["Password"])) {
                   return true;
                } else {
                    throw new Exception('Authentication failed: ' . $smtp->getError()['error'],500);
                }
            }

            return false;
        } catch (Exception $e) {
            $smtp->quit();
            throw $e;
        }
        return false;
    }

    /**
     * @Route("/accounts/{id}", methods={"PUT"})
     */
    public function update_account($id)
    {
//        echo $this->accountsPath."/$id.php";
        if(!file_exists($this->accountsPath."/$id.php"))
            return new Response("Account not found",404);

        if($this->create_account($id))
            return new Response("Account updated",200);
    }

    /**
     * @Route("/accounts/", methods={"POST"})
     */
    public function create_account($update=null)
    {

        $request = Request::createFromGlobals();
        $input = json_decode($request->getContent(),JSON_OBJECT_AS_ARRAY);
        if(!$update) {
            if( !isset($input["id"]))
                return new Response(
                    json_encode(["error" => "Missing ID"]),
                    400,
                    ["Content-Type" => "application/json"]
                );

            $id = $input["id"];
            if(file_exists($this->accountsPath."/".$input["id"].".php"))
                return new Response("Account already exists",409);
        }
        else
            $id = $update;

        $cfg = [
            "SMTPAuth"=>true
        ];

        if(!isset($input["Host"]))
            return new Response(
                json_encode(["error"=>"Missing SMTP Host"]),
                400,
                ["Content-Type"=> "application/json"]
            );
        $cfg["Host"] = $input["Host"];

        if(isset($input["SMTPAuth"]))
            $cfg["SMTPAuth"] = boolval($input["SMTPAuth"]);

        if($cfg["SMTPAuth"]) {
            if(!isset($input["Username"]))
                return new Response(
                    json_encode(["error"=>"Missing Username"]),
                    400,
                    ["Content-Type"=> "application/json"]
                );
            $cfg["Username"] = $input["Username"];

            if(!isset($input["Password"]))
                return new Response(
                    json_encode(["error"=>"Missing Username"]),
                    400,
                    ["Content-Type"=> "application/json"]
                );
            $cfg["Password"] = $input["Password"];
        }

        $security_options = [
            "ssl"=> PHPMailer::ENCRYPTION_SMTPS,
            "tls"=> PHPMailer::ENCRYPTION_STARTTLS
        ];

        if(isset($input["Security"])) {
            if (!in_array($input["Security"], ["ssl", "tls"]))
                return new Response(
                    json_encode(["error" => "Invalid security option"]),
                    400,
                    ["Content-Type"=> "application/json"]
                );
            $cfg["SMTPSecure"] = $security_options[$input["Security"]];
        }

        if(!isset($input["Port"]))
            return new Response(
                json_encode(["error"=>"Missing Port nummber"]),
                400,
                ["Content-Type"=> "application/json"]
            );
        $cfg["Port"] = $input["Port"];

        try {
            $res = $this->smtp_check($cfg);
        }
        catch (Exception $exception) {
            return new Response(
                json_encode(["error"=>$exception->getMessage()]),
                $exception->getCode(),
                ["Content-Type"=> "application/json"]
            );
        }
        $txt = json_encode($cfg);
        $txt = preg_replace(["/\:/","/\{/","/\}/"],["=>","[","]"],$txt);
        file_put_contents($this->accountsPath."/$id.php","<?php\nreturn $txt;");
        if($update)
            return true;

        return new Response(
            null,201
        );
    }
}
